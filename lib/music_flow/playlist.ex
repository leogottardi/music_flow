defmodule MusicFlow.Playlist do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  @foreign_key_type Ecto.UUID

  schema "playlists" do
    field :playlist_name, :string
    belongs_to :user, MusicFlow.User
    has_many :albums, MusicFlow.Album

    timestamps()
  end

  @permited [:playlist_name, :user_id]

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, @permited)
    |> validate_required(@permited)
    |> foreign_key_constraint(:user_id)
  end
end
