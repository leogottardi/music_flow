defmodule MusicFlow.Music.Get do
  alias MusicFlow.{Repo, Music}

  def call(%{"id" => uuid}) do
    case Ecto.UUID.cast(uuid) do
      :error -> {:error, "Invalid UUID"}
      {:ok, uuid} -> fetch_music(uuid)
    end
  end

  defp fetch_music(uuid) do
    case Repo.get(Music, uuid) do
      nil -> {:error, "Music not found!"}
      music -> {:ok, music}
    end
  end
end
