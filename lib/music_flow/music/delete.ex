defmodule MusicFlow.Music.Delete do
  alias MusicFlow.Repo
  alias MusicFlow.Music.Get

  def call(%{"id" => uuid}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, music} -> Repo.delete(music)
      error -> error
    end
  end
end
