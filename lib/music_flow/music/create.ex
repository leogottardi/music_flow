defmodule MusicFlow.Music.Create do
  alias MusicFlow.{Repo, Music}

  def call(params) do
    params
    |> Music.changeset()
    |> handle_create()
  end

  defp handle_create(%Ecto.Changeset{valid?: true} = changeset) do
    changeset
    |> Repo.insert()
  end

  defp handle_create(error), do: {:error, error}
end
