defmodule MusicFlow.Music.Update do
  import Ecto.Changeset

  alias MusicFlow.Repo
  alias MusicFlow.Music.Get

  def call(%{"id" => uuid, "music_name" => music_name}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, music} -> update(music, music_name)
      error -> error
    end
  end

  defp update(music, music_name) do
    music
    |> change(music_name: music_name)
    |> Repo.update()
  end
end
