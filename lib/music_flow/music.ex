defmodule MusicFlow.Music do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  @foreign_key_type Ecto.UUID

  schema "musics" do
    field :music_name
    belongs_to :album, MusicFlow.Album

    timestamps()
  end

  @required [:music_name, :album_id]

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, @required)
    |> validate_required(@required)
    |> foreign_key_constraint(:album_id)
  end
end
