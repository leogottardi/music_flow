defmodule MusicFlow.User do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}

  schema "users" do
    field :name, :string
    field :lastname, :string
    field :cpf, :string
    field :email, :string
    field :password_hash, :string
    field :password, :string, virtual: true
    has_many :playlists, MusicFlow.Playlist

    timestamps()
  end

  @required [:name, :lastname, :cpf, :email, :password]

  def build(params, user_or_module \\ %__MODULE__{}) do
    params
    |> changeset(user_or_module)
  end

  defp changeset(params, user_or_module) do
    user_or_module
    |> cast(params, @required)
    |> validate_required(@required)
    |> validate_format(:email, ~r/@/)
    |> validate_length(:password, min: 6)
    |> validate_length(:cpf, min: 11, max: 11)
    |> unique_constraint(:email)
    |> put_pass_password()
  end

  defp put_pass_password(
         %Ecto.Changeset{changes: %{password: password}, valid?: true} = changeset
       ) do
    change(changeset, Argon2.add_hash(password))
  end

  defp put_pass_password(changeset), do: changeset
end
