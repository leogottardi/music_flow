defmodule MusicFlow.Album.Get do
  alias MusicFlow.{Repo, Album}

  def call(%{"id" => uuid}) do
    case Ecto.UUID.cast(uuid) do
      :error -> {:error, "Invalid UUID"}
      {:ok, uuid} -> fetch_album(uuid)
    end
  end

  defp fetch_album(uuid) do
    case Repo.get(Album, uuid) do
      nil -> {:error, "Album not found!"}
      album -> {:ok, album}
    end
  end
end
