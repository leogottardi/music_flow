defmodule MusicFlow.Album.Delete do
  alias MusicFlow.Repo
  alias MusicFlow.Album.Get

  def call(%{"id" => uuid}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, album} -> Repo.delete(album)
      error -> error
    end
  end
end
