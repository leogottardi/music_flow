defmodule MusicFlow.Album.Update do
  import Ecto.Changeset

  alias MusicFlow.Repo
  alias MusicFlow.Album.Get

  def call(%{"id" => uuid, "album_name" => album_name}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, album} -> update(album, album_name)
      error -> error
    end
  end

  defp update(album, album_name) do
    album
    |> change(album_name: album_name)
    |> Repo.update()
  end
end
