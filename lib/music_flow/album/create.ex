defmodule MusicFlow.Album.Create do
  alias MusicFlow.{Repo, Album}

  def call(params) do
    params
    |> Album.changeset()
    |> handle_create()
  end

  defp handle_create(%Ecto.Changeset{valid?: true} = changeset) do
    changeset
    |> Repo.insert()
  end

  defp handle_create(error), do: {:error, error}
end
