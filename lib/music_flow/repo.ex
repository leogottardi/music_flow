defmodule MusicFlow.Repo do
  use Ecto.Repo,
    otp_app: :music_flow,
    adapter: Ecto.Adapters.Postgres
end
