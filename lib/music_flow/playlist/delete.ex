defmodule MusicFlow.Playlist.Delete do
  alias MusicFlow.Repo
  alias MusicFlow.Playlist.Get

  def call(%{"id" => uuid}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, playlist} -> Repo.delete(playlist)
      error -> error
    end
  end
end
