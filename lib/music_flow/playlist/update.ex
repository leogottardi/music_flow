defmodule MusicFlow.Playlist.Update do
  import Ecto.Changeset

  alias MusicFlow.Repo
  alias MusicFlow.Playlist.Get

  def call(%{"id" => uuid, "playlist_name" => playlist_name}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, playlist} -> update(playlist, playlist_name)
      error -> error
    end
  end

  defp update(playlist, playlist_name) do
    playlist
    |> change(playlist_name: playlist_name)
    |> Repo.update()
  end
end
