defmodule MusicFlow.Playlist.Get do
  alias MusicFlow.{Repo, Playlist}

  def call(%{"id" => uuid}) do
    case Ecto.UUID.cast(uuid) do
      :error -> {:error, "Invalid UUID"}
      {:ok, uuid} -> fetch_playlist(uuid)
    end
  end

  defp fetch_playlist(uuid) do
    case Repo.get(Playlist, uuid) do
      nil -> {:error, "Playlist not found!"}
      playlist -> {:ok, playlist}
    end
  end
end
