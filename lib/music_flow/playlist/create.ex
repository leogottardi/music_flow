defmodule MusicFlow.Playlist.Create do
  alias MusicFlow.{Repo, Playlist}

  def call(params) do
    params
    |> Playlist.changeset()
    |> handle_create()
  end

  defp handle_create(%Ecto.Changeset{valid?: true} = changeset) do
    changeset
    |> Repo.insert()
  end

  defp handle_create(error), do: {:error, error}
end
