defmodule MusicFlow.Album do
  use Ecto.Schema
  import Ecto.Changeset

  @primary_key {:id, Ecto.UUID, autogenerate: true}
  @foreign_key_type Ecto.UUID

  schema "albums" do
    field :album_name, :string
    belongs_to :playlist, MusicFlow.Playlist
    has_many :musics, MusicFlow.Music

    timestamps()
  end

  @required [:album_name, :playlist_id]

  def changeset(params) do
    %__MODULE__{}
    |> cast(params, @required)
    |> validate_required(@required)
    |> foreign_key_constraint(:playlist_id)
  end
end
