defmodule MusicFlow.User.Delete do
  alias MusicFlow.Repo
  alias MusicFlow.User.Get

  def call(%{"id" => uuid}) do
    case Get.call(%{"id" => uuid}) do
      {:ok, user} -> Repo.delete(user)
      error -> error
    end
  end
end
