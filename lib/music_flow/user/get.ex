defmodule MusicFlow.User.Get do
  alias MusicFlow.{Repo, User}

  def call(%{"id" => uuid}) do
    case Ecto.UUID.cast(uuid) do
      :error -> {:error, "Invalid UUID"}
      {:ok, uuid} -> fetch_user(uuid)
    end
  end

  defp fetch_user(uuid) do
    case Repo.get(User, uuid) do
      nil -> {:error, "User not found!"}
      user -> {:ok, user}
    end
  end
end
