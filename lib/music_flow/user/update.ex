defmodule MusicFlow.User.Update do
  alias MusicFlow.User.Get
  alias MusicFlow.{User, Repo}

  def call(%{"id" => uuid} = params) do
    case Get.call(%{"id" => uuid}) do
      {:ok, user} -> update(user, params)
      error -> error
    end
  end

  defp update(user, params) do
    params
    |> User.build(user)
    |> Repo.update()
  end
end
