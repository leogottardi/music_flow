defmodule MusicFlow.User.Create do
  alias MusicFlow.{Repo, User}

  def call(params) do
    params
    |> User.build()
    |> handle_create()
  end

  defp handle_create(%Ecto.Changeset{valid?: true} = changeset) do
    changeset
    |> Repo.insert()
  end

  defp handle_create(error), do: {:error, error}
end
