defmodule MusicFlowWeb.AlbumsController do
  use MusicFlowWeb, :controller

  action_fallback MusicFlowWeb.FallbackController

  def create(conn, params) do
    with {:ok, album} <- MusicFlow.create_album(params) do
      conn
      |> put_status(:created)
      |> render("create.json", album: album)
    end
  end

  def show(conn, params) do
    with {:ok, album} <- MusicFlow.fetch_album(params) do
      conn
      |> put_status(:ok)
      |> render("show.json", album: album)
    end
  end

  def update(conn, params) do
    with {:ok, album} <- MusicFlow.update_album(params) do
      conn
      |> put_status(:ok)
      |> render("update.json", album: album)
    end
  end

  def delete(conn, params) do
    with {:ok, _album} <- MusicFlow.delete_album(params) do
      conn
      |> text("Album deleted")
    end
  end
end
