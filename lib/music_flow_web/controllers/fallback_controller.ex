defmodule MusicFlowWeb.FallbackController do
  use MusicFlowWeb, :controller

  def call(conn, {:error, %Ecto.Changeset{} = result}) do
    conn
    |> put_status(:bad_request)
    |> put_view(MusicFlowWeb.ErrorView)
    |> render("400.json", result: result)
  end

  def call(conn, {:error, message}) do
    conn
    |> put_status(:not_found)
    |> put_view(MusicFlowWeb.ErrorView)
    |> render("401.json", message: message)
  end
end
