defmodule MusicFlowWeb.PlaylistsController do
  use MusicFlowWeb, :controller

  action_fallback MusicFlowWeb.FallbackController

  def create(conn, params) do
    with {:ok, playlist} <- MusicFlow.create_playlist(params) do
      conn
      |> put_status(:created)
      |> render("create.json", playlist: playlist)
    end
  end

  def show(conn, params) do
    with {:ok, playlist} <- MusicFlow.fetch_playlist(params) do
      conn
      |> put_status(:ok)
      |> render("show.json", playlist: playlist)
    end
  end

  def update(conn, params) do
    with {:ok, playlist} <- MusicFlow.update_playlist(params) do
      conn
      |> put_status(:ok)
      |> render("update.json", playlist: playlist)
    end
  end

  def delete(conn, params) do
    with {:ok, _playlist} <- MusicFlow.delete_playlist(params) do
      conn
      |> text("Playlist deleted")
    end
  end
end
