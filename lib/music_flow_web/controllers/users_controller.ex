defmodule MusicFlowWeb.UsersController do
  use MusicFlowWeb, :controller

  action_fallback MusicFlowWeb.FallbackController

  def create(conn, params) do
    with {:ok, user} <- MusicFlow.create_user(params) do
      conn
      |> put_status(:created)
      |> render("create.json", user: user)
    end
  end

  def show(conn, params) do
    with {:ok, user} <- MusicFlow.fetch_user(params) do
      conn
      |> put_status(:ok)
      |> render("show.json", user: user)
    end
  end

  def update(conn, params) do
    with {:ok, user} <- MusicFlow.update_user(params) do
      conn
      |> put_status(:ok)
      |> render("update.json", user: user)
    end
  end

  def delete(conn, params) do
    with {:ok, _user} <- MusicFlow.delete_user(params) do
      conn
      |> text("User deleted")
    end
  end
end
