defmodule MusicFlowWeb.MusicsController do
  use MusicFlowWeb, :controller

  action_fallback MusicFlowWeb.FallbackController

  def create(conn, params) do
    with {:ok, music} <- MusicFlow.create_music(params) do
      conn
      |> put_status(:created)
      |> render("create.json", music: music)
    end
  end

  def show(conn, params) do
    with {:ok, music} <- MusicFlow.fetch_music(params) do
      conn
      |> put_status(:ok)
      |> render("show.json", music: music)
    end
  end

  def update(conn, params) do
    with {:ok, music} <- MusicFlow.update_music(params) do
      conn
      |> put_status(:ok)
      |> render("update.json", music: music)
    end
  end

  def delete(conn, params) do
    with {:ok, _music} <- MusicFlow.delete_music(params) do
      conn
      |> text("Music deleted")
    end
  end
end
