defmodule MusicFlowWeb.ErrorHelpers do
  def translate_error({msg, opts}) do
    if count = opts[:count] do
      Gettext.dngettext(MusicFlowWeb.Gettext, "errors", msg, msg, count, opts)
    else
      Gettext.dgettext(MusicFlowWeb.Gettext, "errors", msg, opts)
    end
  end
end
