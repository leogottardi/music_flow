defmodule MusicFlowWeb.MusicsView do
  use MusicFlowWeb, :view

  def render("create.json", %{music: %{id: id, music_name: music_name, album_id: album_id}}) do
    %{
      message: "Music created!",
      music: %{
        id: id,
        name: music_name,
        album_id: album_id
      }
    }
  end

  def render("show.json", %{music: %{music_name: music_name, album_id: album_id}}) do
    %{
      music: %{
        music_name: music_name,
        album_id: album_id
      }
    }
  end

  def render("update.json", %{music: %{music_name: music_name, album_id: album_id}}) do
    %{
      message: "Music updated!",
      music: %{
        music_name: music_name,
        album_id: album_id
      }
    }
  end
end
