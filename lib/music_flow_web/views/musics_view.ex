defmodule MusicFlowWeb.AlbumsView do
  use MusicFlowWeb, :view

  def render("create.json", %{album: %{id: id, album_name: album_name, playlist_id: playlist_id}}) do
    %{
      message: "Album created!",
      album: %{
        id: id,
        name: album_name,
        playlist_id: playlist_id
      }
    }
  end

  def render("show.json", %{album: %{album_name: album_name, playlist_id: playlist_id}}) do
    %{
      album: %{
        album_name: album_name,
        playlist_id: playlist_id
      }
    }
  end

  def render("update.json", %{album: %{album_name: album_name, playlist_id: playlist_id}}) do
    %{
      message: "Album updated!",
      album: %{
        album_name: album_name,
        playlist_id: playlist_id
      }
    }
  end
end
