defmodule MusicFlowWeb.PlaylistsView do
  use MusicFlowWeb, :view

  def render("create.json", %{playlist: %{id: id, playlist_name: playlist_name, user_id: user_id}}) do
    %{
      message: "Playlist created!",
      playlist: %{
        id: id,
        name: playlist_name,
        user_id: user_id
      }
    }
  end

  def render("show.json", %{playlist: %{playlist_name: playlist_name, user_id: user_id}}) do
    %{
      playlist: %{
        playlist_name: playlist_name,
        user_id: user_id
      }
    }
  end

  def render("update.json", %{playlist: %{playlist_name: playlist_name, user_id: user_id}}) do
    %{
      message: "Playlist updated!",
      playlist: %{
        playlist_name: playlist_name,
        user_id: user_id
      }
    }
  end
end
