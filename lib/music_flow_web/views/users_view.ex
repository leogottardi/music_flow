defmodule MusicFlowWeb.UsersView do
  use MusicFlowWeb, :view

  def render("create.json", %{
        user: %{id: id, name: name, lastname: lastname, email: email, inserted_at: inserted_at}
      }) do
    %{
      message: "User created!",
      user: %{
        id: id,
        name: name,
        lastname: lastname,
        email: email,
        inserted_at: inserted_at
      }
    }
  end

  def render("show.json", %{
        user: %{name: name, lastname: lastname, email: email, inserted_at: inserted_at}
      }) do
    %{
      user: %{
        name: name,
        lastname: lastname,
        email: email,
        inserted_at: inserted_at
      }
    }
  end

  def render("update.json", %{
        user: %{name: name, lastname: lastname, email: email, inserted_at: inserted_at}
      }) do
    %{
      message: "User updated!",
      user: %{
        name: name,
        lastname: lastname,
        email: email,
        inserted_at: inserted_at
      }
    }
  end
end
