defmodule MusicFlowWeb.Router do
  use MusicFlowWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", MusicFlowWeb do
    pipe_through :api

    resources "/user", UsersController, only: [:create, :update, :show, :delete]
    resources "/playlist", PlaylistsController, only: [:create, :update, :show, :delete]
    resources "/album", AlbumsController, only: [:create, :update, :show, :delete]
    resources "/music", MusicsController, only: [:create, :update, :show, :delete]
  end

  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through [:fetch_session, :protect_from_forgery]
      live_dashboard "/dashboard", metrics: MusicFlowWeb.Telemetry
    end
  end
end
