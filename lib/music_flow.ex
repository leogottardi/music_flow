defmodule MusicFlow do
  alias MusicFlow.{User, Playlist, Album, Music}

  defdelegate create_user(params), to: User.Create, as: :call
  defdelegate fetch_user(params), to: User.Get, as: :call
  defdelegate update_user(params), to: User.Update, as: :call
  defdelegate delete_user(params), to: User.Delete, as: :call

  defdelegate create_playlist(params), to: Playlist.Create, as: :call
  defdelegate fetch_playlist(params), to: Playlist.Get, as: :call
  defdelegate update_playlist(params), to: Playlist.Update, as: :call
  defdelegate delete_playlist(params), to: Playlist.Delete, as: :call

  defdelegate create_album(params), to: Album.Create, as: :call
  defdelegate fetch_album(params), to: Album.Get, as: :call
  defdelegate update_album(params), to: Album.Update, as: :call
  defdelegate delete_album(params), to: Album.Delete, as: :call

  defdelegate create_music(params), to: Music.Create, as: :call
  defdelegate fetch_music(params), to: Music.Get, as: :call
  defdelegate update_music(params), to: Music.Update, as: :call
  defdelegate delete_music(params), to: Music.Delete, as: :call
end
