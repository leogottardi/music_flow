defmodule MusicFlow.Repo.Migrations.CreateAlbumTable do
  use Ecto.Migration

  def change do
    create table(:albums, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :title, :string, null: false
      add :artist_id, references(:artists, type: :uuid)

      timestamps()
    end
  end
end
