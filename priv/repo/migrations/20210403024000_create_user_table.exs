defmodule MusicFlow.Repo.Migrations.CreateUserTable do
  use Ecto.Migration

  def change do
    create table(:users, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :name, :string, null: false, null: false
      add :lastname, :string, null: false
      add :cpf, :string, null: false, null: false
      add :email, :string, null: false, null: false
      add :password_hash, :string, null: false

      timestamps()
    end

    create unique_index(:users, [:email])
  end
end
