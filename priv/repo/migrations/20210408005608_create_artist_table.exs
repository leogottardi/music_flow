defmodule MusicFlow.Repo.Migrations.CreateArtistTable do
  use Ecto.Migration

  def change do
    create table(:artists, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :email, :string, null: false
      add :password_hash, :string, null: false
      add :name, :string, null: false
      add :music_style, :string, null: false

      timestamps()
    end
  end
end
