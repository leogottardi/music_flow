defmodule MusicFlow.Repo.Migrations.CreatePlaylistTable do
  use Ecto.Migration

  def change do
    create table(:playlists, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :title, :string, null: false
      add :user_id, references(:users, type: :uuid)

      timestamps()
    end
  end
end
