defmodule MusicFlow.Repo.Migrations.CreateMusicTable do
  use Ecto.Migration

  def change do
    create table(:musics, primary_key: :false) do
      add :id, :uuid, primary_key: true
      add :title, :string, null: false
      add :album_id, references(:albums, type: :uuid)
      add :artist_id, references(:artists, type: :uuid), null: true

      timestamps()
    end

  end
end
