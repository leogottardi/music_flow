defmodule MusicFlow.Repo.Migrations.CreatePlaylistMusicsTable do
  use Ecto.Migration

  def change do
    create table(:playlist_musics, primary_key: false) do
      add :id, :uuid, primary_key: true
      add :music_id, references(:musics, type: :uuid)
      add :playlist_id, references(:playlists, type: :uuid)
    end
  end
end
