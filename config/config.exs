use Mix.Config

config :music_flow,
  ecto_repos: [MusicFlow.Repo]

config :music_flow, MusicFlowWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "ckhftZLzLMh0vR4HIqMi7mu3u6mDup4ktqNzGgUk9X4XSKNOJQuhTCMDDm9f/MQi",
  render_errors: [view: MusicFlowWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: MusicFlow.PubSub,
  live_view: [signing_salt: "Y8nZI/J0"]

config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

config :phoenix, :json_library, Jason

import_config "#{Mix.env()}.exs"
