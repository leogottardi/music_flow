use Mix.Config

config :music_flow, MusicFlow.Repo,
  username: "postgres",
  password: "postgres",
  database: "music_flow_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

config :music_flow, MusicFlowWeb.Endpoint,
  http: [port: 4002],
  server: false

config :logger, level: :warn
